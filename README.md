# jrnlwarrior - taskwarrior and jrnl cross-pollination

This little script allows simple interaction between `taskwarrior` and a `jrnl` file.
It parses the `jrnl` file and transfers accomplished and unfinished tasks to `taskwarrior`,
as well as adding today's tasks as an entry to the `jrnl` file.

More specifically, it:

* transfers completed tasks to `taskwarrior` by logging them and removes the lines from this file\
(marked with `[x] ` or `x ` at the beginning of the line)
* transfers incomplete tasks to `taskwarrior` by adding them as new tasks scheduled at the date of the entry they appear under\
(marked with an empty `[ ] ` at the beginning of their line)
* adds an entry for today's date if none exists yet and populates it with due/overdue tasks from `taskwarrior`

To accomplish this it borrows a little from the [todo.txt](http://todotxt.org/) syntax --- 
namely the idea of (A) (B) (C) prioritization and `x task done syntax` 
(i.e. starting a line with `x ` or `[x] ` means it represents an accomplished task).

All three of these operations *only* operate on entries which are marked with a special title (`todotxt` by default),
though this can be changed through a regex option.
That means, it is also entirely possible to have other entries (with the same date) which will simply be ignored by the script.

Lastly, it transfers what I call 'ideas' to `taskwarrior`.
These are small tidbits and notions I jot down during the day (hence, 'ideas')
and marked as such by starting the line either with `idea: ` or the `taskwarrior` equivalent `+idea `.
I want them transferred to `taskwarrior` so they don't get lost overall, 
but they should be 
a) not bound to a specific date, even when written in a specific `jrnl` entry, and
b) specifically marked as vague ideas in `taskwarrior`.
For now, they will be applied the fixed tags `+idea +maybe` in `taskwarrior` and simply 
transferred date-less.

An example `jrnl` file:

```jrnl
[2021-10-30 10:16] todotxt
This is just a test entry. It shows how tasks in todo blocks are handled.
[ ] This is an incomplete task - it will be transferred directly to tw.
[x] This is a completed task - it will be logged as completed in tw.
[ ] (A) This is an incomplete task which will get assigned high priority in taskwarrior.
x This is also a completed task, as long as the x is the first thing on the line with a space behind.
x All four of these tasks will be transferred to tw, and their lines deleted from this file.

idea: This is an idea and it will be transferred to tw with corresponding tags, then deleted from this file.

This is not a task but a normal line and will remain in the file.
xAs will this,
[ ]As will this.

[ ] This will be transferred again, however.

[2021-10-29 10:16] Another entry
This is a regular jrnl entry, since it is missing the correct title.
By default it looks for `todotxt` titles, 
but the title can be freely set for the script (`-b` option).
jrnlwarrior will not look for incomplete or completed tasks within this block at all.
[x] This is a completed task, but it will not be transferred to taskwarrior.
[ ] Neither will this incomplete task.
+idea This idea however *will* be transferred, since ideas are looked for in *all* entries.
```

## Usage

Point the file to your `jrnl` file (ideally it needs to be a single file) and 
set the syntax which declares a `todo` entry within it.

```bash
./jrnlwarrior.py -f ~/.local/share/jrnl/journal.txt -b 'todotxt'
```

The command above sets the script to work on a specific `jrnl` file
and only work on entries which have exactly `todotxt` as their title.
The settings above are also the default settings of the script.

**BE AWARE** that this script *actively changes* your `jrnl` file during normal operations.
If you are afraid of destructive operations, you have two options:
Create a backup of your `jrnl` file before running this script to be able to `diff` the two afterwards.
Or, to see what the program would do without actually implementing any changes,
invoke dry run mode:

```bash
./jrnlwarrior.py -n
```

It will spit out a list of tasks that are added to `taskwarrior`, 
as well as its own options,
lines to be removed from the file and any to-do entries added to the file.
It will not change your `jrnl` file in any way.

If you want to switch off one of the three ways this script connects the two,
you can use the `-L`, `-I`, `-T`, `-D` options which turn off [*L*]ogging completed to `taskwarrior`, [*I*]nserting incomplete to `taskwarrior`, creating new `jrnl` entries for [*T*]oday's tasks, or moving hatched i[*D*]eas to `taskwarrior` respectively.

```bash
./jrnlwarrior.py -DIT
```

The above invocation will *only* log completed tasks to `taskwarrior`. 
Adding new to-dos, ideas and creating today entries is turned off.

```bash
./jrnlwarrior.py -T
```

This will create a one-way connection to `taskwarrior`, 
by transferring both new and completed tasks and ideas to the program.
However, tasks to be done today will not be transferred back to this file.

## Scope

This was a fun weekend project and it shines through --- 
the code got increasingly spaghetti towards the end (especially entry-adding to `jrnl` file was one Sunday night hour),
it's not packaged particularly well
and there are no tests of any kind implemented.

I am happily using the script in my daily workflow, 
however some assumptions are made and edge cases will happen at some point.
Please don't run it willy-nilly on a long-treasured `jrnl` file without having proper backups.
Otherwise, there is no clear road-map --- 
mostly, I think, the code should be cleaned up and a way found that handles `taskwarrior` task duplication better (with `uuid` or `description` comparisons or similar means).

I will probably not be able to devote much more time to this in the foreseeable future,
but if you find an issue tell me nonetheless or, even better, 
see if you can spot how to fix it and I will gladly merge the changes in!

