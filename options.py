import os
import sys
import argparse
from dataclasses import dataclass, field
from typing import List


@dataclass
class Options:
    jrnl_fname: str = field(
        default_factory=lambda: f"{os.environ.get('XDG_DATA_HOME', os.environ.get('HOME'))}/jrnl/journal.txt"
    )
    todo_block_title: str = "todotxt"
    dryrun: bool = False
    taskwarrior_log_completed: bool = True
    taskwarrior_add_incomplete: bool = True
    taskwarrior_fill_today: bool = True
    taskwarrior_add_ideas: bool = True

    # can not yet be changed
    taskwarrior_overrides: List = field(
        default_factory=lambda: [
            "rc.context=none",
            "rc.verbose=nothing",
            "rc.hooks=0",
        ]
    )
    regex_task_completed: str = r"(?:x|\[x\])"
    regex_task_incomplete: str = r"\[ \]"
    regex_task_idea: str = r"(?:idea:|\+idea)"


def init():
    opt = Options()

    parser = argparse.ArgumentParser()
    create_cmdline_args(parser)
    parse_cmdline_args(parser, opt)
    if opt.dryrun:
        dryrun_show_options(opt)
    validate_opts(opt)
    return opt


def dryrun_show_options(options):
    print(options)


def create_cmdline_args(parser):
    parser.add_argument(
        "-n", "--dryrun", help="only simulate and print changes", action="store_true"
    )
    parser.add_argument("-f", "--file", help="the jrnl file to ingest")
    parser.add_argument("-b", "--blocktitle", help="the title of entries to parse")
    parser.add_argument(
        "-L",
        "--nologging",
        help="don't log done todo items to taskwarrior",
        action="store_true",
    )
    parser.add_argument(
        "-I",
        "--noincomplete",
        help="don't add incomplete todo items to taskwarrior",
        action="store_true",
    )
    parser.add_argument(
        "-T",
        "--nofilltoday",
        help="don't add today's todo items as entry to file",
        action="store_true",
    )
    parser.add_argument(
        "-D",
        "--noidea",
        help="don't add ideas as maybe items to taskwarrior",
        action="store_true",
    )


def parse_cmdline_args(parser, options):
    args = parser.parse_args()

    if args.dryrun:
        options.dryrun = True
    if args.nologging:
        options.taskwarrior_log_completed = False
    if args.noincomplete:
        options.taskwarrior_add_incomplete = False
    if args.noidea:
        options.taskwarrior_add_ideas = False
    if args.nofilltoday:
        options.taskwarrior_fill_today = False
    if args.file:
        options.jrnl_fname = args.file
    if args.blocktitle:
        options.todo_block_title = args.blocktitle

    return options


def validate_opts(options):
    if not os.path.isfile(options.jrnl_fname):
        print(f"{options.jrnl_fname} does not seem to be a file. Aborting.")
        sys.exit(1)
